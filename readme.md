## Create master and worker nodes with vagrant

`$ vagrant up`

## Setup ansible vault encryption

- move the the playbooks' directory: `$ cd ansible-playbooks`

- Generate vault encryption: `$ ansible-vault encrypt_string --vault-id ansibleuser@prompt --name 'ansible_password'`

- With the output provided from the above mentioned command replace the value for `ansible_password` and `ansible_become_password` in the `inventory.yaml` file.

- To install all the dependencies and get the nodes ready form a cluster run: 
`$ ansible-playbook kube-dependencies.yaml -i inventory/inventory.yaml  --vault-id ansibleuser@prompt`

- And to create the master node and join worker nodes run the following command:
`$ ansible-playbook cluster-up.yaml -i inventory/inventory.yaml  --vault-id ansibleuser@prompt`